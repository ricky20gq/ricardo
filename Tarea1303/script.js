function Consultar()
{
    let registro;
    registro= sessionStorage.getItem(document.getElementById("Registro").value);
    document.getElementById("Nombre").value = JSON.parse(registro).Nombre;
    document.getElementById("Apellido").value= JSON.parse(registro).Apellido;
}
function Agregar()
{
    let nombre,registro,apellido;
    let JsonLocal;
    nombre = document.getElementById("Nombre").value;
    registro = document.getElementById("Registro").value;
    apellido = document.getElementById("Apellido").value;
    JsonLocal = {
      "Nombre":  nombre,
      "Apellido":apellido            
    }
    sessionStorage.setItem(registro,JSON.stringify(JsonLocal));
    Blanqueo();
}
function Eliminar()
{
    let registro;
    registro = document.getElementById("Registro").value;
    sessionStorage.removeItem(registro);
    Blanqueo();
}
function Limpiar()
{
    sessionStorage.clear();
    Blanqueo();
}
function Blanqueo()
{
    document.getElementById("Nombre").value = "";
    document.getElementById("Registro").value="";
    document.getElementById("Apellido").value="";
}